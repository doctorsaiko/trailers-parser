Парсер трейлеров из itunes
=====================================

Приложение парсит последние 10 трейлеров (заголовок, описание, постер, ссылка на источник) в rss ленте из apple itunes movies через консольную команду. 
  
Результат можно посмотреть на главной странице проекта



Для того чтобы развернуть в docker
=====================================

1. docker-compose build
2. make up 
3. make composer-install
4. make db-init
5. make fetch-trailers

Инициализация приложение при локальном деплое
=====================================

1. composer install
2. php bin/console orm:schema-tool:update --force
3. php bin/console fetch:trailers