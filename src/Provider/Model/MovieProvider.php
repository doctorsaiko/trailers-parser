<?php


namespace App\Provider\Model;


use App\Container\Container;
use App\Flusher;
use App\Movie\Entity\MovieRepository;
use App\Movie\UseCase\Create;
use App\Support\ServiceProviderInterface;
use Doctrine\ORM\EntityManagerInterface;
use Monolog\Logger;
use Psr\Container\ContainerInterface;

class MovieProvider implements ServiceProviderInterface
{
    public function register(Container $container): void
    {
        $this->defineRepositories($container);
        $this->defineHandlers($container);
    }

    private function defineHandlers(Container $container): void
    {
        $container->set(Create\Handler::class, static function (ContainerInterface $container) {
            return new Create\Handler($container->get(MovieRepository::class), $container->get(Flusher::class), $container->get(Logger::class));
        });
    }

    private function defineRepositories(Container $container): void
    {
        $container->set(MovieRepository::class, static function (ContainerInterface $container) {
            return new MovieRepository($container->get(EntityManagerInterface::class));
        });
    }
}