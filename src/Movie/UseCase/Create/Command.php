<?php


namespace App\Movie\UseCase\Create;

use Symfony\Component\Validator\Constraints as Assert;

class Command
{
    /**
     * @Assert\All(
     *     constraints={
     *      @Assert\Collection(
     *     fields={
     *      "title"={
     *          @Assert\NotBlank,@Assert\Length(min=1, max=255),@Assert\Required
     *      },
     *     "link"={
     *          @Assert\NotBlank,@Assert\Length(min=1, max=255),@Assert\Url,@Assert\Required
     *      },
     *     "description"={
     *          @Assert\NotBlank,@Assert\Length(min=1, max=4294967295),@Assert\Required
     *      },
     *     "pub_date"={
     *          @Assert\NotBlank,@Assert\Required,@Assert\Length(max=255)
     *      }
     *    }
     * )
     * }
     * )
     * @Assert\NotBlank
     *
     * @var array
     */
    public array $trailers;
}