<?php


namespace App\Movie\UseCase\Create;


use App\Flusher;
use App\Movie\Entity\Movie;
use App\Movie\Entity\MovieRepository;
use DateTime;
use Exception;
use Monolog\Logger;
use RuntimeException;

class Handler
{
    private MovieRepository $movies;
    private Flusher $flusher;
    private Logger $logger;
    private bool $logMessages = false;

    public function __construct(MovieRepository $movies, Flusher $flusher, Logger $logger)
    {
        $this->movies = $movies;
        $this->flusher = $flusher;
        $this->logger = $logger;
    }

    /**
     * @throws Exception
     */
    public function handle(Command $command, bool $logMessages = false)
    {
        $this->logMessages = $logMessages;

        foreach ($command->trailers as $trailer) {
            $movie = $this->getMovie($trailer['title'])
                ->setTitle($trailer['title'])
                ->setDescription($trailer['description'])
                ->setLink($trailer['link'])
                ->setPubDate(new DateTime($trailer['pub_date']))
                ->setImage($trailer['link'] . '/images/poster.jpg');
            $this->movies->add($movie);
        }

        $this->flusher->flush();
    }

    protected function getMovie(string $title): Movie
    {
        $item = $this->movies->findByTitle($title);

        if ($this->logMessages) {
            if ($item === null) {
                $this->logger->info('Create new Movie', ['title' => $title]);
            } else {
                $this->logger->info('Move found', ['title' => $title]);
            }
        }

        if (!$item) {
            $item = new Movie();
        }

        return $item;
    }
}