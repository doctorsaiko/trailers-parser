<?php declare(strict_types=1);

namespace App\Movie\Entity;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;


class MovieRepository
{
    private EntityRepository $repo;
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->repo = $em->getRepository(Movie::class);
        $this->em = $em;
    }

    public function add(Movie $movie): void
    {
        $this->em->persist($movie);
    }

    public function findByTitle(string $title): ?Movie
    {
        return $this->repo->findOneBy(['title' => $title]);
    }
}
