<?php declare(strict_types=1);

namespace App\Command;

use App\Movie\UseCase\Create;
use Cassandra\Exception\ValidationException;
use Exception;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use function GuzzleHttp\Psr7\str;

class FetchDataCommand extends Command
{
    private const SOURCE = 'https://trailers.apple.com/trailers/home/rss/newtrailers.rss';

    protected static $defaultName = 'fetch:trailers';

    private ClientInterface $httpClient;
    private LoggerInterface $logger;
    private Create\Handler $handler;
    private ValidatorInterface $validator;

    /**
     * FetchDataCommand constructor.
     *
     * @param ClientInterface $httpClient
     * @param LoggerInterface $logger
     * @param Create\Handler $handler
     * @param string|null $name
     */
    public function __construct(ClientInterface $httpClient, LoggerInterface $logger, ValidatorInterface $validator, Create\Handler $handler, string $name = null)
    {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->handler = $handler;
        $this->validator = $validator;
    }

    public function configure(): void
    {
        $this
            ->setDescription('Fetch data from iTunes Movie Trailers')
            ->addArgument('source', InputArgument::OPTIONAL, 'Overwrite source');
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info(sprintf('Start %s at %s', __CLASS__, (string)date_create()->format(DATE_ATOM)));
        $source = self::SOURCE;
        if ($input->getArgument('source')) {
            $source = $input->getArgument('source');
        }

        if (!is_string($source)) {
            throw new RuntimeException('Source must be string');
        }
        $io = new SymfonyStyle($input, $output);
        $io->title(sprintf('Fetch data from %s', $source));

        try {
            $response = $this->httpClient->sendRequest(new Request('GET', $source));
        } catch (ClientExceptionInterface $e) {
            throw new RuntimeException($e->getMessage());
        }
        if (($status = $response->getStatusCode()) !== 200) {
            throw new RuntimeException(sprintf('Response status is %d, expected %d', $status, 200));
        }
        $data = $response->getBody()->getContents();
        $this->processXml($data);

        $this->logger->info(sprintf('End %s at %s', __CLASS__, (string)date_create()->format(DATE_ATOM)));

        return 0;
    }

    /**
     * @throws Exception
     */
    protected function processXml(string $data): void
    {
        $xml = (new \SimpleXMLElement($data))->children();

        if (!property_exists($xml, 'channel')) {
            throw new RuntimeException('Could not find \'channel\' element in feed');
        }

        $trailers = [];

        for ($i = 0; $i < 10; $i++) {
            $item = $xml->channel->item[$i];
            $trailers[$i]['title'] = (string) $item->title;
            $trailers[$i]['description'] = (string)$item->description;
            $trailers[$i]['link'] = (string)$item->link;
            $trailers[$i]['pub_date'] = (string)$item->pubDate;
        }

        $command = new Create\Command();
        $command->trailers = $trailers;

        $violations = $this->validator->validate($command);

        if ($violations->count() > 0) {
            echo $violations;
            throw new RuntimeException('Type Error!');
        }

        $command->trailers = $trailers;

        $this->handler->handle($command);
    }
}
