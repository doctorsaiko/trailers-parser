up: docker-up
down: docker-down
restart: down up

docker-up:
		docker-compose up -d
docker-down:
		docker-compose down --remove-orphans
docker-build:
		docker-compose build --pull

fetch-trailers:
	docker-compose run --rm -w /var/www/app php-fpm php bin/console fetch:trailers

db-update:
	docker-compose run --rm -w /var/www/app php-fpm php bin/console orm:schema-tool:update --force

composer-install:
	docker-compose run --rm -w /var/www/app php-fpm composer install

composer-update:
	docker-compose run --rm -w /var/www/app php-fpm composer update

test-unit:
	docker-compose run --rm -w /var/www/app php-fpm composer test -- --testsuite=unit

test-all:
	docker-compose run --rm -w /var/www/app php-fpm composer test